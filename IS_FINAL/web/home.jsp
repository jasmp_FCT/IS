<%-- 
    Document   : index.jsp
    Created on : May 1, 2014, 3:36:09 PM
    Author     : Joaquim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>IS RJJ - Enterprise Manager Version 0.1</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link href="css/dashboard.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>

        <%
            String UserName = "";
            String role = "";
            if (session.getAttribute("user") == null || session.getAttribute("role") == null) {
                response.sendRedirect("login.jsp");
            } else {
                UserName = session.getAttribute("user").toString();
                role = session.getAttribute("role").toString();
            }
        %>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">IS Manager</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="home.jsp">Home</a></li>
                        <li><a href="#" value="home" id="logoutBT">Logout (<% out.print(UserName);%>)</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">


                <div class="col-sm-3 col-md-2 sidebar">

                    <ul class="nav nav-sidebar">
                        <li><a href="EnterprisePages/ClientSide.jsp">My Account</a></li>
                            <%if (role.equalsIgnoreCase("admin")) { %>
                        <li><a href="EnterprisePages/EnterpriseSide.jsp">Enterprise</a></li>
                            <%}%>
                    </ul>
                </div>

                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">HOME</h1>
                    <div class="row">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Made By :</h3>
                            </div>
                            <div class="panel-body">
                                <p>Joaquim Pereira Nº32295 MIEEC</p>
                                <p>Renato Paulino Nº32865 MIEEC</p>
                                <p>João Soares Nº32497 MIEEC</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <script src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script src="js/logoutJS.js" type="text/javascript"></script>
    </body>
</html>
