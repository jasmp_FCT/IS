<%-- 
    Document   : index.jsp
    Created on : May 1, 2014, 3:36:09 PM
    Author     : Joaquim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>IS RJJ - Enterprise Manager Version 0.1</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link href="../css/dashboard.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>

        <%
            String UserName = "";
            String role = "";
            if (session.getAttribute("user") == null || session.getAttribute("role") == null) {
                response.sendRedirect("../login.jsp");
            } else {
                UserName = session.getAttribute("user").toString();
                role = session.getAttribute("role").toString();
            }

            if (role.equals("user")) {
                response.sendRedirect("ClientSide.jsp");
            }
        %>


        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                    </button>
                    <a class="navbar-brand" href="../home.jsp">IS Manager</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="../home.jsp">Home</a></li>
                        <li><a href="#" id="logoutBT">Logout (<% out.print(UserName);%>)</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">


                <div class="col-sm-3 col-md-2 sidebar">

                    <ul class="nav nav-sidebar">
                        <li><a href="ClientSide.jsp">My Account</a></li>
                            <%if (role.equalsIgnoreCase("admin")) { %>
                        <li class="active"><a href="EnterpriseSide.jsp">Enterprise</a></li>
                            <%}%>
                    </ul>
                </div>


                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">Enterprise Operations</h1>



                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Generate DataBase</h3>
                                </div>
                                <div class="panel-body">
                                    <div id="resultDiv" style="visibility: hidden"></div>
                                    <form id="dbGenerate" action="../EnterpriseServlet" method="GET">
                                        <input type="text" name="btSelect" value="DB" style="display : none"/>
                                        <button id="createDBclick" class="btn btn-lg btn-danger btn-block" type="submit">Generate Database</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Change User Role</h3>
                                </div>
                                <div class="panel-body">
                                    <div id="resultDiv2" style="visibility: hidden"></div>
                                    <form id="changeRoleForm" action="../EnterpriseServlet" method="POST">
                                        UserName: <input type="text" name="UserNameChoose">
                                        Role:   <select id="roleDD" name="roleChoose" required="">
                                        </select>
                                        <input type="submit" class="btn btn-success" id="btRole" value="ChangeRole">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Search Client History</h3>
                            </div>
                            <div class="panel-body">
                                <form id="searchForm" action="../EnterpriseServlet" method="GET">
                                    User Name: <input type="text" name="User_Name">
                                    <input type="text" name="btSelect" value="SearchUser" style="display : none"/>
                                    <input type="submit" />
                                </form>
                            </div>
                        </div>
                    </div>

                    <div id="tableSearch" class="row table-responsive"></div>

                    <div class="row">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Search domestic appliance History</h3>
                            </div>
                            <div class="panel-body">
                                <form id="searchFormSR" action="../EnterpriseServlet" method="GET">
                                    Serial Number: <input type="text" name="Serial_Number">
                                    <input type="text" name="btSelect" value="SearchSerial" style="display : none"/>
                                    <input type="submit" />
                                </form>
                            </div>
                        </div>
                    </div>

                    <div id="tableSearchElectro" class="row table-responsive"></div>



                </div>
            </div>
        </div>
        <script src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="js/ClientSearchServlets.js" type="text/javascript"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script src="../js/logoutJS.js" type="text/javascript"></script>
    </body>
</html>
