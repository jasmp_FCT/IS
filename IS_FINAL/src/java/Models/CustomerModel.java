/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Joaquim
 */
public class CustomerModel {

    private int LoginRoleID;
    private String userName;
    private String password;
    private String Name;
    private int Telephone;
    private String Residence;

    public int getLoginRoleID() {
        return LoginRoleID;
    }

    public void setLoginRoleID(int LoginRoleID) {
        this.LoginRoleID = LoginRoleID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getTelephone() {
        return Telephone;
    }

    public void setTelephone(int Telephone) {
        this.Telephone = Telephone;
    }

    public String getResidence() {
        return Residence;
    }

    public void setResidence(String Residence) {
        this.Residence = Residence;
    }

}
