/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * @author Joaquim
 */
public class DeviceUsages {            

    public String getTime() {
        return time;
    }

    public DeviceUsages(String time, String program_description, String water_temperature) {
        this.time = time;
        this.program_description = program_description;
        this.water_temperature = water_temperature;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getProgram_description() {
        return program_description;
    }

    public void setProgram_description(String program_description) {
        this.program_description = program_description;
    }

    public String getWater_temperature() {
        return water_temperature;
    }

    public void setWater_temperature(String water_temperature) {
        this.water_temperature = water_temperature;
    }
    private String time;
    private String program_description;
    private String water_temperature;
}
