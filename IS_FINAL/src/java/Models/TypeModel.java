/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * @author Joaquim
 */
public class TypeModel {
    private int type_UID;
    private String type_Description;

    public TypeModel(int type_UID, String type_Description) {
        this.type_UID = type_UID;
        this.type_Description = type_Description;
    }

    public int getType_UID() {
        return type_UID;
    }

    public void setType_UID(int type_UID) {
        this.type_UID = type_UID;
    }

    public String getType_Description() {
        return type_Description;
    }

    public void setType_Description(String type_Description) {
        this.type_Description = type_Description;
    }
    
}
