/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebServices;

import DataBase.DataBaseManagement;
import Models.RolesModel;
import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Joaquim
 */
@WebService(serviceName = "EnterpriseWS")
@Stateless()
public class EnterpriseWS {

    private final DataBaseManagement db = new DataBaseManagement();

    @WebMethod(operationName = "getCustomerUsages")
    public ArrayList<String> getCustomerUsages(@WebParam(name = "userName") String userName) {
        return db.getCustomerUsages(userName);
    }

    @WebMethod(operationName = "getDeviceUsages")
    public ArrayList<String> getDeviceUsages(@WebParam(name = "serialNumber") String serialNumber) {
        return db.getDeviceUsages(serialNumber);
    }

    @WebMethod(operationName = "getDeviceTypeUsages")
    public ArrayList<String> getDeviceTypeUsages(int typeID) {
        return db.getDeviceTypeUsages(typeID);
    }

    @WebMethod(operationName = "UpdateUserRole")
    public boolean UpdateUserRole(@WebParam(name = "userName")String userName,@WebParam(name = "loginRole") int loginRole) {
        return db.updateCustomer(userName,loginRole);
    }
    
    @WebMethod(operationName = "getAllRoles")
    public ArrayList<RolesModel> getALLRoles(){
        return db.getALLRoles();
    }
}
