/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.CustomersUsages;
import Models.DeviceUsages;
import Models.MyDevices;
import Models.ProgramsModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Joaquim
 */
public class Clients {

    public static List<String> getCustomerUsages(String userName) {
        webServicesClient2.EnterpriseWS_Service service = new webServicesClient2.EnterpriseWS_Service();
        webServicesClient2.EnterpriseWS port = service.getEnterpriseWSPort();
        return port.getCustomerUsages(userName);
    }

    public static List<String> getDeviceUsages(String serialNumber) {
        webServicesClient2.EnterpriseWS_Service service = new webServicesClient2.EnterpriseWS_Service();
        webServicesClient2.EnterpriseWS port = service.getEnterpriseWSPort();
        return port.getDeviceUsages(serialNumber);
    }

    public static List<String> getMyDevices(String userName) {
        webServicesClient3.ClientWS_Service service = new webServicesClient3.ClientWS_Service();
        webServicesClient3.ClientWS port = service.getClientWSPort();
        return port.getMyDevices(userName);
    }

    public static List<MyDevices> structMyDevices(List<String> query) {
        List<MyDevices> ret = new ArrayList<>();

        Iterator<String> it = query.iterator();
        int i = 0;
        String aux1 = "";

        while (it.hasNext()) {
            i++;
            switch (i) {
                case 1:
                    aux1 = it.next();
                    break;
                case 2:
                    ret.add(new MyDevices(it.next(), aux1));
                    i = 0;
                    break;
            }

        }

        return ret;
    }

    public static List<CustomersUsages> convertFromStringArray(List<String> query) {
        List<CustomersUsages> ret = new ArrayList<>();

        Iterator<String> it = query.iterator();
        int i = 0;
        int serial_number = -1;
        String date = "", program_description = "", type = "";

        while (it.hasNext()) {
            i++;
            switch (i) {
                case 1:
                    date = it.next();
                    break;
                case 2:
                    serial_number = Integer.parseInt(it.next());
                    break;
                case 3:
                    type = it.next();
                    break;
                case 4:
                    program_description = it.next();
                    break;
                case 5:
                    ret.add(new CustomersUsages(date, serial_number, program_description, type, Integer.parseInt(it.next())));
                    i = 0;
                    break;
            }

        }

        return ret;
    }

    public static List<DeviceUsages> convert2FromStringArray(List<String> query) {
        List<DeviceUsages> ret = new ArrayList<>();

        Iterator<String> it = query.iterator();
        int i = 0;
        String aux1 = "", aux2 = "";

        while (it.hasNext()) {
            i++;
            switch (i) {
                case 1:
                    aux1 = it.next();
                    break;
                case 2:
                    aux2 = it.next();
                    break;
                case 3:
                    ret.add(new DeviceUsages(aux1, aux2, it.next()));
                    i = 0;
                    break;
            }

        }

        return ret;
    }

    public static List<webServicesClient3.TypeModel> getallTypes() {
        webServicesClient3.ClientWS_Service service = new webServicesClient3.ClientWS_Service();
        webServicesClient3.ClientWS port = service.getClientWSPort();
        return port.getallTypes();
    }

    public static boolean associateDevice(int arg0, String arg1, String arg2, String arg3, int arg4) {
        webServicesClient3.ClientWS_Service service = new webServicesClient3.ClientWS_Service();
        webServicesClient3.ClientWS port = service.getClientWSPort();
        return port.associateDevice(arg0, arg1, arg2, arg3, arg4);
    }

    public static List<webServicesClient2.RolesModel> getAllRoles() {
        webServicesClient2.EnterpriseWS_Service service = new webServicesClient2.EnterpriseWS_Service();
        webServicesClient2.EnterpriseWS port = service.getEnterpriseWSPort();
        return port.getAllRoles();
    }

    public static boolean updateUserRole(String userName, int loginRole) {
        webServicesClient2.EnterpriseWS_Service service = new webServicesClient2.EnterpriseWS_Service();
        webServicesClient2.EnterpriseWS port = service.getEnterpriseWSPort();
        return port.updateUserRole(userName, loginRole);
    }

    public static List<String> getProgams() {
        webServicesClient3.ClientWS_Service service = new webServicesClient3.ClientWS_Service();
        webServicesClient3.ClientWS port = service.getClientWSPort();
        return port.getProgams();
    }

    public static List<ProgramsModel> structPrograms(List<String> query) {
        List<ProgramsModel> ret = new ArrayList<>();

        Iterator<String> it = query.iterator();
        int i = 0;
        String aux1 = "";

        while (it.hasNext()) {
            i++;
            switch (i) {
                case 1:
                    aux1 = it.next();
                    break;
                case 2:
                    ret.add(new ProgramsModel(aux1, it.next()));
                    i = 0;
                    break;
            }

        }

        return ret;
    }

    public static boolean operateDevice(int serialNumber, int programID, int temperature) {
        webServicesClient3.ClientWS_Service service = new webServicesClient3.ClientWS_Service();
        webServicesClient3.ClientWS port = service.getClientWSPort();
        return port.operateDevice(serialNumber, programID, temperature);
    }
    
    

}
