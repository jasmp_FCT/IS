$(document).ready(function() {
    $.ajax({
        type: 'GET',
        url: '../ClientServlet',
        data: {opt: "default"},
        success: function(rjson) { // Execute Ajax GET request on URL of "someservlet" and execute the following function with Ajax response JSON...
            $('#searchtab').remove();
            var $table = $('<table>').appendTo($('#resultelectro')).addClass('table table-striped').attr("id", 'searchtab'); // Create HTML <table> element and append it to HTML DOM element with ID "somediv".
            $table.append("<thead><tr>")
                    .append("<th>Serial Number</th>")
                    .append("<th>Friendly Name</th>");
            $.each(rjson, function(index, user) {    // Iterate over the JSON array.
                $('<tr>').appendTo($table)                     // Create HTML <tr> element, set its text content with currently iterated item and append it to the <table>.
                        .append($('<td>').text(user.serialNumber))        // Create HTML <td> element, set its text content with id of currently iterated product and append it to the <tr>.
                        .append($('<td>').text(user.frindlyName));
            });
        }
    });

    $('#associarBT').click(function(e) {
        e.preventDefault();

        $.ajax({
            type: "GET",
            url: '../ClientServlet',
            data: {opt: "getTypes"},
            success: function(rjson) {
                $('#ddDeviceType').empty();
                $.each(rjson, function(index, user) {
                    $('#ddDeviceType').append($('<option>').text(user.typeDescription).attr('value', user.typeUID));
                });
            }
        });

        $('#associarDiv').lightbox_me({
            centered: true,
            onLoad: function() {
                $('#associarDiv').find('input:first').focus();
            }
        });

    });

    $('#operarBT').click(function(e) {
        e.preventDefault();

        $.ajax({
            type: "GET",
            url: '../ClientServlet',
            data: {opt: "getPrograms"},
            success: function(rjson) {
                $('#ddprograms').empty();
                $.each(rjson, function(index, user) {
                    $('#ddprograms').append($('<option>').text(user.description).attr('value', user.id));
                });
            }
        });

        $('#operarDiv').lightbox_me({
            centered: true,
            onLoad: function() {
                $('#operarDiv').find('input:first').focus();
            }
        });

    });


    $('#associateForm').submit(function(event) {
        event.preventDefault();

        $.ajax({
            type: $('#associateForm').attr('method'),
            url: $('#associateForm').attr('action'),
            data: $('#associateForm').serialize(),
            success: function(plainText) {
                if (plainText.toString().search("NOT") !== -1) {
                    $('#resultDiv').text(plainText).css('visibility', 'visible').addClass('alert alert-danger');
                } else {
                    $('#resultDiv').text(plainText).css('visibility', 'visible').addClass('alert alert-success');
                }
                $('#associarDiv').trigger('close');

                window.setTimeout(function() {
                    window.location.href = 'ClientSide.jsp';
                }, 1000);
            }
        });
    });

    $('#operarForm').submit(function(event) {
        event.preventDefault();

        $.ajax({
            type: $('#operarForm').attr('method'),
            url: $('#operarForm').attr('action'),
            data: $('#operarForm').serialize(),
            success: function(plainText) {
                if (plainText.toString().search("NOT") !== -1) {
                    $('#resultDiv').text(plainText).css('visibility', 'visible').addClass('alert alert-danger');
                } else {
                    $('#resultDiv').text(plainText).css('visibility', 'visible').addClass('alert alert-success');
                }
                $('#operarDiv').trigger('close');

                window.setTimeout(function() {
                    window.location.href = 'ClientSide.jsp';
                }, 1000);
            }
        });
    });

});