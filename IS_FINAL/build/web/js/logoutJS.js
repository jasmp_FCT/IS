$("#logoutBT").click(function(e) {
    e.preventDefault();
    
    if ($(this).attr('value') === "home") {
        var request = $.ajax({
            url: "logoutServlet",
            type: "POST"
        });

        request.done(function() {
            window.location = 'login.jsp';
        });
    }
    else {
        var request = $.ajax({
            url: "../logoutServlet",
            type: "POST"
        });

        request.done(function() {
            window.location = '../login.jsp';
        });
    }
});
