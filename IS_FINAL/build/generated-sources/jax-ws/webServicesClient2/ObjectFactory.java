
package webServicesClient2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webServicesClient2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAllRolesResponse_QNAME = new QName("http://WebServices/", "getAllRolesResponse");
    private final static QName _GetDeviceUsagesResponse_QNAME = new QName("http://WebServices/", "getDeviceUsagesResponse");
    private final static QName _GetAllRoles_QNAME = new QName("http://WebServices/", "getAllRoles");
    private final static QName _UpdateUserRoleResponse_QNAME = new QName("http://WebServices/", "UpdateUserRoleResponse");
    private final static QName _GetCustomerUsages_QNAME = new QName("http://WebServices/", "getCustomerUsages");
    private final static QName _GetDeviceTypeUsagesResponse_QNAME = new QName("http://WebServices/", "getDeviceTypeUsagesResponse");
    private final static QName _UpdateUserRole_QNAME = new QName("http://WebServices/", "UpdateUserRole");
    private final static QName _GetDeviceUsages_QNAME = new QName("http://WebServices/", "getDeviceUsages");
    private final static QName _GetDeviceTypeUsages_QNAME = new QName("http://WebServices/", "getDeviceTypeUsages");
    private final static QName _GetCustomerUsagesResponse_QNAME = new QName("http://WebServices/", "getCustomerUsagesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webServicesClient2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllRolesResponse }
     * 
     */
    public GetAllRolesResponse createGetAllRolesResponse() {
        return new GetAllRolesResponse();
    }

    /**
     * Create an instance of {@link GetAllRoles }
     * 
     */
    public GetAllRoles createGetAllRoles() {
        return new GetAllRoles();
    }

    /**
     * Create an instance of {@link GetDeviceUsagesResponse }
     * 
     */
    public GetDeviceUsagesResponse createGetDeviceUsagesResponse() {
        return new GetDeviceUsagesResponse();
    }

    /**
     * Create an instance of {@link UpdateUserRoleResponse }
     * 
     */
    public UpdateUserRoleResponse createUpdateUserRoleResponse() {
        return new UpdateUserRoleResponse();
    }

    /**
     * Create an instance of {@link GetCustomerUsages }
     * 
     */
    public GetCustomerUsages createGetCustomerUsages() {
        return new GetCustomerUsages();
    }

    /**
     * Create an instance of {@link GetDeviceTypeUsagesResponse }
     * 
     */
    public GetDeviceTypeUsagesResponse createGetDeviceTypeUsagesResponse() {
        return new GetDeviceTypeUsagesResponse();
    }

    /**
     * Create an instance of {@link GetDeviceTypeUsages }
     * 
     */
    public GetDeviceTypeUsages createGetDeviceTypeUsages() {
        return new GetDeviceTypeUsages();
    }

    /**
     * Create an instance of {@link UpdateUserRole }
     * 
     */
    public UpdateUserRole createUpdateUserRole() {
        return new UpdateUserRole();
    }

    /**
     * Create an instance of {@link GetDeviceUsages }
     * 
     */
    public GetDeviceUsages createGetDeviceUsages() {
        return new GetDeviceUsages();
    }

    /**
     * Create an instance of {@link GetCustomerUsagesResponse }
     * 
     */
    public GetCustomerUsagesResponse createGetCustomerUsagesResponse() {
        return new GetCustomerUsagesResponse();
    }

    /**
     * Create an instance of {@link RolesModel }
     * 
     */
    public RolesModel createRolesModel() {
        return new RolesModel();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllRolesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getAllRolesResponse")
    public JAXBElement<GetAllRolesResponse> createGetAllRolesResponse(GetAllRolesResponse value) {
        return new JAXBElement<GetAllRolesResponse>(_GetAllRolesResponse_QNAME, GetAllRolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceUsagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getDeviceUsagesResponse")
    public JAXBElement<GetDeviceUsagesResponse> createGetDeviceUsagesResponse(GetDeviceUsagesResponse value) {
        return new JAXBElement<GetDeviceUsagesResponse>(_GetDeviceUsagesResponse_QNAME, GetDeviceUsagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllRoles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getAllRoles")
    public JAXBElement<GetAllRoles> createGetAllRoles(GetAllRoles value) {
        return new JAXBElement<GetAllRoles>(_GetAllRoles_QNAME, GetAllRoles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "UpdateUserRoleResponse")
    public JAXBElement<UpdateUserRoleResponse> createUpdateUserRoleResponse(UpdateUserRoleResponse value) {
        return new JAXBElement<UpdateUserRoleResponse>(_UpdateUserRoleResponse_QNAME, UpdateUserRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerUsages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getCustomerUsages")
    public JAXBElement<GetCustomerUsages> createGetCustomerUsages(GetCustomerUsages value) {
        return new JAXBElement<GetCustomerUsages>(_GetCustomerUsages_QNAME, GetCustomerUsages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceTypeUsagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getDeviceTypeUsagesResponse")
    public JAXBElement<GetDeviceTypeUsagesResponse> createGetDeviceTypeUsagesResponse(GetDeviceTypeUsagesResponse value) {
        return new JAXBElement<GetDeviceTypeUsagesResponse>(_GetDeviceTypeUsagesResponse_QNAME, GetDeviceTypeUsagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "UpdateUserRole")
    public JAXBElement<UpdateUserRole> createUpdateUserRole(UpdateUserRole value) {
        return new JAXBElement<UpdateUserRole>(_UpdateUserRole_QNAME, UpdateUserRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceUsages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getDeviceUsages")
    public JAXBElement<GetDeviceUsages> createGetDeviceUsages(GetDeviceUsages value) {
        return new JAXBElement<GetDeviceUsages>(_GetDeviceUsages_QNAME, GetDeviceUsages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceTypeUsages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getDeviceTypeUsages")
    public JAXBElement<GetDeviceTypeUsages> createGetDeviceTypeUsages(GetDeviceTypeUsages value) {
        return new JAXBElement<GetDeviceTypeUsages>(_GetDeviceTypeUsages_QNAME, GetDeviceTypeUsages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerUsagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getCustomerUsagesResponse")
    public JAXBElement<GetCustomerUsagesResponse> createGetCustomerUsagesResponse(GetCustomerUsagesResponse value) {
        return new JAXBElement<GetCustomerUsagesResponse>(_GetCustomerUsagesResponse_QNAME, GetCustomerUsagesResponse.class, null, value);
    }

}
